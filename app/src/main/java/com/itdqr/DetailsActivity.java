package com.itdqr;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DetailsActivity extends AppCompatActivity {
    private Button btnAspirants;
    private String urlAspirants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        urlAspirants = "http://www.itdurango.edu.mx/aspirantes.html";
        btnAspirants = findViewById(R.id.btnAspirants);
        btnAspirants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlAspirants));
                startActivity(intent);
            }
        });
    }
}
